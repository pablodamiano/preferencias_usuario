import 'package:flutter/material.dart';
import 'package:preferenciasusuario/src/share_prefers/preferencias_ususario.dart';
import 'package:preferenciasusuario/src/widgets/menu_widgets.dart';

class HomePage extends StatelessWidget {

  static final String homepage = 'home';
  final prefs = new PreferenciasUsuario();
  @override
  Widget build(BuildContext context) {
    prefs.ultimaPagina = HomePage.homepage;
    return Scaffold(
      appBar: AppBar(
        backgroundColor:(prefs.colorSecundario)?Colors.teal : Colors.blue,
        centerTitle: true,
        title: Text('Preferencias de Usurio'),
      ),
      drawer: MenuWidget(),
      body:Column(
        mainAxisAlignment: MainAxisAlignment.center,
          children: [
          Text('Color Secundario:${prefs.colorSecundario}'),
          Divider(),
          Text('Genero:${prefs.genero}'),
          Divider(),
          Text('Nombre Usuario:${prefs.nombreUsuario}'),
          Divider()
        ],
      ),
    );
  }


}