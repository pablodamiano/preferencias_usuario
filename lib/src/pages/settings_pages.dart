import 'package:flutter/material.dart';
import 'package:preferenciasusuario/src/share_prefers/preferencias_ususario.dart';
import 'package:preferenciasusuario/src/widgets/menu_widgets.dart';


class SettingsPage extends StatefulWidget {

  static final String settingsPage = 'settingsPage';

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {

  bool   _secundario;
  int    _genero;
  String _nombre     =  'Pedro';
  TextEditingController _controller;
  final prefs = PreferenciasUsuario();

 @override
  void initState() {
    prefs.ultimaPagina = SettingsPage.settingsPage;
    // TODO: implement initState
    super.initState();
    _genero = prefs.genero;
    _secundario = prefs.colorSecundario;
    _controller = TextEditingController(text:prefs.nombreUsuario);
  }

  setSelectedRadio(int value)async {
    prefs.genero = value;
     _genero = value;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor:(prefs.colorSecundario)?Colors.teal : Colors.blue,
        centerTitle: true,
        title: Text('Settings'),
      ),
      body:ListView(
        children: [
          Container(
            padding:  EdgeInsets.all(5.0),
            child: Text('SETTINGS',style: TextStyle(fontSize: 45.0,fontWeight: FontWeight.bold),),

          ),
          Divider(),
          SwitchListTile(
            title: Text('Color Secundario'),
            value: _secundario ,
            onChanged:(value){
             setState(() {
                _secundario = value;
                prefs.colorSecundario = value;
             });
            }
          ),
          Divider(),
          RadioListTile(
            title: Text('Masculino'),
            value: 1, 
            groupValue: _genero, 
            onChanged:setSelectedRadio,
          ),
          Divider(),
          RadioListTile(
            title: Text('Femenino'),
            value: 2, 
            groupValue: _genero, 
            onChanged:setSelectedRadio,
          ),
          Divider(),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 40.0),
            child: TextField(
              controller: _controller,
              decoration: InputDecoration(
                labelText: 'Nombre  ',
                helperText: 'Nombre de la persona usando el telefono'
              ),
              onChanged: (value){
               prefs.nombreUsuario = value;
              },
            ),
          ),
        ],
      ),
      drawer: MenuWidget(),
    );
  }
}