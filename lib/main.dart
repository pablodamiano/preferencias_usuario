import 'package:flutter/material.dart';
import 'package:preferenciasusuario/src/pages/home_pages.dart';
import 'package:preferenciasusuario/src/pages/settings_pages.dart';
import 'package:preferenciasusuario/src/share_prefers/preferencias_ususario.dart';
 
void main()async{
  
  WidgetsFlutterBinding.ensureInitialized();

  final prefs = new  PreferenciasUsuario();
  await prefs.initPrefs();

  runApp(MyApp());
}
 
class MyApp extends StatelessWidget {
  final prefs = new  PreferenciasUsuario();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Preferences User',
      initialRoute: prefs.ultimaPagina,
      routes: {
        HomePage.homepage          : (BuildContext context) => HomePage(),
        SettingsPage.settingsPage  : (BuildContext context) => SettingsPage(),
      },
    );
  }
}